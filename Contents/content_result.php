  <style type="text/css">
      .container {
  width: 300px;
  clear: both;
}

.container input {
  width: 100%;
  clear: both;
}

body{ font: 14px Georgia, serif; }
 
#page-wrap { width: 500px; margin: 0 auto;}
 
h1  { margin: 25px 0; font: 18px Georgia, Serif; text-transform: uppercase; letter-spacing: 3px; }
 
#quiz input {
    vertical-align: middle;
}
 
#quiz ol {
   margin: 0 0 10px 20px;
}
 
#quiz ol li {
   margin: 0 0 20px 0;
}
 
#quiz ol li div {
   padding: 4px 0;
}
 
#quiz h3 {
   font-size: 17px; margin: 0 0 1px 0; color: #666;
}
 
#results {
    font: 44px Georgia, Serif;
}
  </style>

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <?php include 'Contents/header_result.php'; ?>

    
 <div id="page-wrap">
        
        <?php

            $answer1 = $_POST['question-1-answers'];
            $answer2 = $_POST['question-2-answers'];
            $answer3 = $_POST['question-3-answers'];
            $answer4 = $_POST['question-4-answers'];
            $answer5 = $_POST['question-5-answers'];
        
            $totalCorrect = 0;
            
            if ($answer1 == "C") { $totalCorrect++; }
            if ($answer2 == "D") { $totalCorrect++; }
            if ($answer3 == "A") { $totalCorrect++; }
            if ($answer4 == "B") { $totalCorrect++; }
            if ($answer5 == "D") { $totalCorrect++; }

            
            echo "<div id='results'>$totalCorrect / 5 správně</div>";
            
        ?>
    
    </div>
  
  </div>
    <?php include 'Contents/ads.php'; ?>

